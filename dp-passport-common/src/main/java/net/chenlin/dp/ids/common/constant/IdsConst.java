package net.chenlin.dp.ids.common.constant;

/**
 * ids系统静态常量
 * @author zcl<yczclcn@163.com>
 */
public class IdsConst {

    /** 登录成功session参数 **/
    public static final String SESSION_PARAM_KEY = "sessionId";

    /** session数据key **/
    public static final String SESSION_KEY = "sessionData";

    /** redis 会话key **/
    public static final String REDIS_SESSION_KEY = "dp:ids:";

    /** cookie过期时间：默认2小时 60*60*24*7 **/
    public static final int COOKIE_MAX_AGE = 604800;

    /** cookie保存路径：默认根路径 **/
    public static final String COOKIE_PATH = "/";

    /** redis默认过期时间：默认30分钟 60*30 **/
    public static final int REDIS_EXPIRE_TIME = 1800;

    /** 登录地址 **/
    public static final String LOGIN_URL = "/login";

    /** 登出地址 **/
    public static final String LOGOUT_URL = "/logout";

    /** 校验地址 **/
    public static final String AUTH_STATUS_URL = "/authStatus";

    /** 刷新session过期时间地址 **/
    public static final String AUTH_REFRESH_URL = "/refreshStatus";

    /** 删除session地址 **/
    public static final String AUTH_REMOVE_URL = "/removeStatus";

    /** 重定向地址参数名称 **/
    public static final String REDIRECT_KEY = "service";

    /** 成功错误码 **/
    public static final String SUCCESS_CODE = "0000";

    /** 系统异常错误码 **/
    public static final String ERROR_CODE = "9999";

    /** 业务异常错误码 **/
    public static final String BIZ_ERR_CODE = "1000";

    /** remember me 状态 **/
    public static final String REMEMBER_ON = "on";

    /** web登录端 **/
    public static final int LOGIN_TYPE_WEB = 1;

    /** app登录端 **/
    public static final int LOGIN_TYPE_APP = 2;

    /** 匿名访问策略 **/
    public static final String ANON_ACCESS_POLICY = "ANON_ACCESS";

    /** 授权访问策略 **/
    public static final String AUTH_ACCESS_POLICY = "AUTH_ACCESS";

    /** 路由访问策略 **/
    public static final String ROUTER_ACCESS_POLICY = "ROUTER_ACCESS";

}
